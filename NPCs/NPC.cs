﻿using Terraria.ModLoader;

namespace asdf.NPCs
{
    public abstract class NPC : ModNPC
    {
        public string name;
        
        public int maxHealth;
        public int defense;
        
        public AI.BaseAI ai;

        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault(name);
        }

        public override void SetDefaults()
        {
            npc.lifeMax = maxHealth;
            npc.defense = defense;
        }

        public override void AI()
        {
            ai.onAITick(npc);
        }
    }
}