﻿using System;
using System.Collections.Generic;
using Terraria.ModLoader;

namespace asdf.Registeries
{
    public class RecipeRegistery
    {
        private static List<ModRecipe> recipes = new List<ModRecipe>();
        public static Mod mod;

        public static ModRecipe registerRecipe(Dictionary<int, int> ingredients, int result, int craftingStation)
        {
            ModRecipe recipe = new ModRecipe(mod);
            foreach (KeyValuePair<int, int> ingredient in ingredients)
            {
                recipe.AddIngredient(ingredient.Key, ingredient.Value);
            }
            recipe.SetResult(result);
            recipe.AddTile(craftingStation);
            recipes.Add(recipe);
            
            return recipe;
        }
    }
}